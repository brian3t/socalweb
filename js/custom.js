const SOCALWEBAPI = 'https://usvsolutions.com/wapi/'

var $win = $(window), $body = $('body'), $header = $('#header'), y_offsetWhenScrollDisabled, $offset, successSubmit, unlockScroll, lockScroll, $flagFancy = false;

var loadJS = function (url, implementationCode, location){
  var scriptTag = document.createElement('script');
  scriptTag.src = url;

  scriptTag.onload = implementationCode;
  scriptTag.onreadystatechange = implementationCode;

  location.appendChild(scriptTag);
};
successSubmit = async function (form_e){
  const f = $('#free_consult')
  const is_form_valid = f[0].reportValidity()
  if (!is_form_valid) return false
  form_e = $(form_e).closest('form')
  form_e[0].reportValidity()
  $flagFancy = true;
  parent.jQuery.fancybox.getInstance().close();
  let $form_params = flat_array_to_assoc($(form_e).serializeArray()) //name, email, msg, agree_terms
  if (!$form_params.hasOwnProperty('agree_terms') || $form_params.agree_terms != 'on') {
    alert(`Please agree terms and conditions, thank you`)
    return false
  }
  const params = {act: 'mail', from: $form_params.email, to: 'someids@gmail.com', cont: `From socalwebapp ${$form_params.msg}`}
  const mail_res = await fetch(SOCALWEBAPI + 'j.php?' + new URLSearchParams(params), {
    method: 'GET', mode: 'cors'
  })
  console.info(`mail_res`, mail_res)
  if (!(mail_res.status) || mail_res.status !== 200) {
    alert(`Error sending email. Please call our phone number`)
    return false
  }
  $.fancybox.open({
    padding: 0, src: '#succsesOrder',

    transitionIn: 'fade', transitionOut: 'fade', padding: 0, margin: 0, speedIn: 0, speedOut: 0, smallBtn: true, toolbar: false, btnTpl: {
      smallBtn: '<button type="button" data-fancybox-close class="fancybox-button fancybox-close-small"><i class="icon-close"></i></button>'
    }, onInit: function (){
      setTimeout(function (){
        $('.js-stickly:not(.sticksy-dummy-node)').attr('style', $styles)
      })
    }, beforeShow: function (){
      if (! $flagFancy) {
        lockScroll();
      }
    }, afterClose: function (){

      if (! $flagFancy) {
        unlockScroll();

        setTimeout(function (){
          $('.js-stickly:not(.sticksy-dummy-node)').attr('style', $styles)
        })
      }
      $flagFancy = false
    }
  });
}

$win.on('load', function (){

  $(".js-img").each(function (){
    var $el = $(this);
    $el.attr('src', $el.data("src"));
  });

  $(".js-bg").each(function (){
    $(this).css('background-image', 'url(' + $(this).data("preload") + ')');
  });
  $body.removeClass('loaded');
  if ($(".js-lazy").length) {
    var lazyLoadInstance = new LazyLoad({
      elements_selector: ".js-lazy"
    });

    if (lazyLoadInstance) {
      lazyLoadInstance.update();
    }
  }
  //  code phone
  var $time2 = 700;
  if ($body.is('.ios')) {
    $time2 = 3000;
  }
  //mask tel
  if ($('[type="tel"]').length > 0) {
    setTimeout(function (){
      loadJS('js/components/maskedinput.js', maskFunc, document.body);

      function maskFunc(){
        $('[type="tel"]').inputmask({
          mask: "+9(999) 999 99 99", showMaskOnHover: false
        });
      }
    }, $time2);

  }
});
$(function (){

  $(".js-menu").each(function (e){
    const $e = $(e.target)
    $(this).singlePageNav({
      offset: $header.outerHeight(), images: 'img.js-lazy:not(.loaded)'
    });
  });
  $(".js-menu a").on('click', function (){
    const $el = $(this)
    if (!$el instanceof jQuery) return false
    const target = $el.attr('target')
    if (!target || target < ' ') return
    const href = $el.attr('href')
    if (!href || href < ' ') return false
    window.open(href, target) //e.g. _blank
  });
/*
  $(".js-menu-footer a:not(.current)").on('click', function (){
    $('.js-menu').not($(this).parent()).find('li').eq($(this).parent().index()).find('a').trigger('click')
  })
*/

  function getScrollbarWidth(){
    const outer = document.createElement('div');
    outer.style.visibility = 'hidden';
    outer.style.overflow = 'scroll';
    outer.style.msOverflowStyle = 'scrollbar';
    document.body.appendChild(outer);
    const inner = document.createElement('div');
    outer.appendChild(inner);
    const scrollbarWidth = (outer.offsetWidth - inner.offsetWidth);
    outer.parentNode.removeChild(outer);
    return scrollbarWidth;

  }

  var $flagFix = false;
  var $flagFixO = false, y_offsetWhenScrollDisabled
  $win.scroll(function (){
    y_offsetWhenScrollDisabled = $win.scrollTop();
    if (y_offsetWhenScrollDisabled > 0) {
      $('#header').addClass('fixed');
      $flagFix = true;
    } else {
      if (! $body.is('.open-header') && ! $flagFixO) {
        $('#header').removeClass('fixed');
        $flagFix = false;

      }
    }
  });

  var $offsetTop;
  lockScroll = function (){
    if ($flagFix) {
      $flagFixO = true;
    }
    $flagFix2 = true;
    $offsetTop = y_offsetWhenScrollDisabled;
    $('html').css('margin-top', -$offsetTop);
    $body.addClass('scrollDisabled');
    $header.addClass('fixed');

    $width = getScrollbarWidth()
    $('body, html').css('padding-right', $width)
  }

  unlockScroll = function (){


    $('html').css('margin-top', '');
    $body.removeClass('scrollDisabled');
    $('body, html').css('padding-right', '')
    setTimeout(function (){

      $('html, body').animate({
        scrollTop: $offsetTop
      }, 0);

    })
    $flagFixO = false
  }


  if ($('.js-fancybox').length) {

    $(".js-fancybox").fancybox({
      padding: 0, margin: 20, touch: false, animationEffect: 'zoom', transitionEffect: "zoom-in-out", speed: 350, transitionDuration: 300, smallBtn: true, clickOutside: "close", btnTpl: {
        smallBtn: '<button type="button" data-fancybox-close class="fancybox-button fancybox-close-small"><i class="icon-close"></i></button>'
      }, init: function (){

        lockScroll();
      }, afterClose: function (){

        unlockScroll();
      }
    })
  }


  /* placeholder*/
  $('input, textarea').each(function (){
    var placeholder = $(this).attr('placeholder');
    $(this).focus(function (){
      $(this).attr('placeholder', '');
    });
    $(this).focusout(function (){
      $(this).attr('placeholder', placeholder);
    });
  });
  /* placeholder*/


  var swiper1;
  if ($('.js-slider-1').length > 0) {
    var swiper = new Swiper('.js-slider-1', {
      loop: false, spaceBetween: 0, slidesPerView: 1, speed: 400,

      effect: 'fade', autoplay: {
        delay: 2500, disableOnInteraction: false,
      },

    });


  }
  // toggle mob menu
  $('.js-button-nav').click(function (){
    var $el = $(this);
    $el.toggleClass('active');
    $('body').toggleClass('open-header');
    $('#mainNav').toggleClass('active');
    if ($('body').is('.open-header')) {
      lockScroll();
    } else {
      setTimeout(function (){
        unlockScroll();
      }, 400);
    }
    return false;
  });

  // datepicker
  if ($("#datepicker").length > 0) {
    $("#datepicker").datepicker({
      dayNamesMin: ["M", "T", "W", "T", "F", "S", "S"]
    });
  }

});


/**
 * Convert jQuery's serializeArray() array into assoc array
 * Also merge input of the same name into array, e.g. union_memberships = Agent & union_memberships = Other
 * becomes union_memberships = [Agent, Other]
 * Also parse money value
 * @param arr
 * @returns assoc array, e.g. {'name': 'John', 'age': 22, 'array': ['a','b'] }
 */
function flat_array_to_assoc(arr){
  if (! _.isArray(arr)) {
    return {};
  }
  let result = {};
  arr.forEach((e) => {
    if (_.isObject(e)) {
      e = _.toArray(e);
      let key = e[0];
      if (e.length === 2) // ["first_name", "John"]
      {
        let val = e[1];
        if (typeof val === 'string') {
          val = val.replace('$', '');
        }
        if (isNumeric(val)) {
          val = Number(val.replace(/[^0-9\.]+/g, ''));
          val = parseFloat(val);
        }
        if (! _.has(result, key)) {
          result[key] = val;
        } else {
          if (_.isString(result[key])) {
            result[key] = new Array(result[key]);
          }
          result[key].push(val);
        }

      }
    }
  });
  return result;
}

/**
 * Plain javascript isNumeric
 **/
function isNumeric(n){
  const parsed = parseFloat(n);
  const parsed_string = parsed.toString();//100.5
  //check if parsed_string == n; //here n is 100.50 preg must discard trailing zero after dot
  const parsed_string_int_decimal = parsed_string.split('.');
  if (n === null) {
    return false;
  }
  const n_int_decimal = n.toString().split('.');
  if (parsed_string_int_decimal.length !== n_int_decimal.length) {
    return false;
  }
  if (n_int_decimal[0] !== parsed_string_int_decimal[0]) {
    return false;
  }
  if (parsed_string_int_decimal.length === 2) {
    //remove trailing zero from decimal
    const parsed_decimal = parsed_string_int_decimal[1].replace(/([1-9]+)0+/gi, '$1');
    const n_decimal = n_int_decimal[1].replace(/([1-9]+)0+/gi, '$1');
    if (n_decimal !== parsed_decimal) {
      return false;
    }
  }

  return ! isNaN(parsed) && isFinite(parsed);
}
