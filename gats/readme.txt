Thanks for buying :)


The documentation is in the "documentation" folder. 
The HTML is in the "HTML" folder. 
RTL version is in the "HTML-RTL" folder.
Updates are in the changelog.txt file.

All psd files are well-organized and labeled which makes the customization process more smooth and painless.


If you have any question please contact me via http://velikorodnov.ticksy.com/

If you like our product, don�t forget to rate it. Thank you! :)